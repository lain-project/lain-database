CREATE TABLE public.auditoria (
	id uuid NOT NULL,
	evento varchar NULL,
	estado varchar NULL,
	payload varchar NULL,
	usuario_id int4 NULL,
	fecha_creacion timestamp NULL,
	fecha_actualizacion timestamp NULL,
	CONSTRAINT auditoria_pk PRIMARY KEY (id),
	CONSTRAINT auditoria_fk FOREIGN KEY (usuario_id) REFERENCES public.users(id)
);