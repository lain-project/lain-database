CREATE TABLE public.unidades (
	id uuid NOT NULL DEFAULT uuid_generate_v4(),
	numero int4 NULL,
	nombre varchar NULL,
	objetivos varchar NULL,
	estado bool NULL,
	fecha_creacion timestamp NULL DEFAULT now(),
	fecha_actualizacion timestamp NULL DEFAULT now(),
	usuario_creador int4 NULL,
	usuario_actualizador int4 NULL,
	materia_id uuid NULL,
	nodo_id varchar NULL,
	CONSTRAINT unidades_pk PRIMARY KEY (id),
	CONSTRAINT unidades_fk FOREIGN KEY (materia_id) REFERENCES public.subjects(id)
);