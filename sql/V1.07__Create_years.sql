CREATE TABLE public.years (
	id serial4 NOT NULL,
	"name" varchar NULL,
	numeration int4 NULL,
	CONSTRAINT years_pk PRIMARY KEY (id)
);


INSERT INTO public.years (id, "name",numeration) VALUES
	 (DEFAULT, 'Primer Año',1),
	 (DEFAULT, 'Segundo Año',2),
	 (DEFAULT, 'Tercer Año',3),
	 (DEFAULT, 'Cuarto Año',4),
	 (DEFAULT, 'Quinto Año',5),
	 (DEFAULT, 'Sexto Año',6),
	 (DEFAULT, 'Septimo Año',7),
	 (DEFAULT, 'Octavo Año',8),
	 (DEFAULT, 'Noveno Año',9),
	 (DEFAULT, 'Decimo Año',10);