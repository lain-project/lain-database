CREATE TABLE public.profiles(
    id SERIAL CONSTRAINT profile_pk PRIMARY KEY,
    lastname VARCHAR NOT NULL,
    firstname VARCHAR NOT NULL,
    position VARCHAR
);

INSERT INTO public.profiles (id, lastname, firstname, position) 
VALUES (DEFAULT, 'Riperto', 'Aaron', 'Titular');