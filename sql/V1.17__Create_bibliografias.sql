CREATE TABLE public.bibliografias (
	id uuid NOT NULL DEFAULT uuid_generate_v4(),
	nombre varchar NULL,
	isbn varchar NULL,
	estado bool NULL DEFAULT true,
	nodo_id varchar NULL,
	fecha_creacion timestamp NULL DEFAULT now(),
	fecha_actualizacion timestamp NULL DEFAULT now(),
	usuario_creado int4 NULL,
	usuario_actualizador int4 NULL,
	materia_id uuid NULL,
	autor varchar NULL,
	CONSTRAINT bibliografias_pk PRIMARY KEY (id),
	CONSTRAINT bibliografias_fk FOREIGN KEY (materia_id) REFERENCES public.subjects(id)
);