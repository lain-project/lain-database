CREATE TABLE public.contenidos (
	id uuid NOT NULL DEFAULT uuid_generate_v4(),
	nombre varchar NULL,
	descripcion varchar NULL,
	minimo bool NULL DEFAULT true,
	estado bool NULL DEFAULT true,
	unidad_id uuid NULL,
	nodo_id varchar NULL,
	fecha_creacion timestamp NULL DEFAULT now(),
	fecha_actualizacion timestamp NULL DEFAULT now(),
	usuario_creador int4 NULL,
	usuario_actualizador int4 NULL,
	CONSTRAINT contenidos_pk PRIMARY KEY (id),
	CONSTRAINT contenidos_fk FOREIGN KEY (unidad_id) REFERENCES public.unidades(id)
);