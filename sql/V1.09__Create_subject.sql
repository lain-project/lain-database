CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE public.subjects (
	id uuid DEFAULT uuid_generate_v4 (),
	image varchar NULL,
	"name" varchar NULL,
	code varchar NULL,
	regime varchar NULL,
	weekly_hours int4 NULL,
	total_hours int4 NULL,
	study_plan_year int4 NULL,
	node_id varchar NULL,
	CONSTRAINT subjects_pk PRIMARY KEY (id),
	CONSTRAINT subjects_fk FOREIGN KEY (study_plan_year) REFERENCES public.study_plans_years(id)
);