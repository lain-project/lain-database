CREATE TABLE public.schemas(
	id serial4 NOT NULL,
	name varchar NULL,
	schema_id varchar NULL,
	study_plan_id int NULL,
	CONSTRAINT schemas_pk PRIMARY KEY (id),
	CONSTRAINT schemas_fk FOREIGN KEY (study_plan_id) REFERENCES public.study_plans(id)
);