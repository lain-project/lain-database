CREATE TABLE public.rols(
    id SERIAL CONSTRAINT rols_pk PRIMARY KEY,
    name VARCHAR NOT NULL,
    description VARCHAR,
    status INTEGER
);

INSERT INTO public.rols (id, name, description, status)
VALUES (DEFAULT, 'ADMIN', 'administrador del sistema', 1);

INSERT INTO public.rols (id, name, description, status)
VALUES (DEFAULT, 'DOCENTE', 'docente de la universidad', 1);