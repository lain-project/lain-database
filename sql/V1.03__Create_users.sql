CREATE TABLE public.users (
    id SERIAL CONSTRAINT users_pk PRIMARY KEY,
    username VARCHAR NOT NULL,
    email VARCHAR,
    password VARCHAR,
    avatar VARCHAR,
    google BOOLEAN,
    status INTEGER,
    rol_id INTEGER NOT NULL CONSTRAINT rol_fk REFERENCES rols,
    profile_id INTEGER NOT NULL CONSTRAINT profile_fk REFERENCES profiles,
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    created_by INTEGER,
    updated_by INTEGER
);
INSERT INTO public.users (
        id,
        username,
        email,
        password,
        avatar,
        google,
        status,
        rol_id,
        created_at,
        updated_at,
        created_by,
        updated_by,
        profile_id
    )
VALUES (
        DEFAULT,
        'himura',
        'aaron.riperto@gmail.com',
        '$2b$12$IhMmgAgnQ7CG9cGn3VTX5eAdqVrGTgEpewG5dSvliYxohcwIgv5vK',
        'img',
        false,
        1,
        1,
        NOW(),
        NOW(),
        null,
        null,
        1
    )