CREATE TABLE public.nodos_materias (
	id uuid NOT NULL,
	materia_id uuid NULL,
	nodo_id varchar NULL,
	nombre varchar NULL,
	CONSTRAINT nodos_materias_pk PRIMARY KEY (id),
	CONSTRAINT nodos_materias_fk FOREIGN KEY (materia_id) REFERENCES public.subjects(id)
);