CREATE TABLE public.study_plans(
	id serial4 NOT NULL,
	code varchar NULL,
	name varchar NULL,
	description varchar NULL,
	workspace_id varchar NULL,
	CONSTRAINT study_plans_pk PRIMARY KEY (id)
);