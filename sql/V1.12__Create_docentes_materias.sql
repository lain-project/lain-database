CREATE TABLE public.docentes_materias (
	id uuid NOT NULL DEFAULT uuid_generate_v4(),
	docente_id int4 NULL,
	materia_id uuid NULL,
	fecha_creacion timestamp NULL DEFAULT now(),
	fecha_actualizacion timestamp NULL DEFAULT now(),
	usuario_creador int4 NULL,
	usuario_actualizador int4 NULL,
	nodo_id varchar NULL,
	estado bool NULL DEFAULT true,
	CONSTRAINT docentes_materias_pk PRIMARY KEY (id),
	CONSTRAINT docentes_materias_fk FOREIGN KEY (docente_id) REFERENCES public.users(id),
	CONSTRAINT docentes_materias_fk_1 FOREIGN KEY (materia_id) REFERENCES public.subjects(id),
	CONSTRAINT docentes_materias_fk_2 FOREIGN KEY (usuario_creador) REFERENCES public.users(id),
	CONSTRAINT docentes_materias_fk_3 FOREIGN KEY (usuario_actualizador) REFERENCES public.users(id)
);