CREATE TABLE public.correlativas (
	id uuid NOT NULL DEFAULT uuid_generate_v4(),
	nombre varchar NULL,
	id_relacion_neural varchar NULL,
	cursar int4 NULL,
	"final" int4 NULL,
	estado bool NULL DEFAULT true,
	materia_inicial uuid NULL,
	materia_final uuid NULL,
	fecha_creacion timestamp NULL DEFAULT now(),
	fecha_actualizacion timestamp NULL DEFAULT now(),
	usuario_creador int4 NULL,
	usuario_actualizador int4 NULL,
	CONSTRAINT correlativas_pk PRIMARY KEY (id),
	CONSTRAINT materia_final_fk FOREIGN KEY (materia_final) REFERENCES public.subjects(id),
	CONSTRAINT materia_inicial_fk FOREIGN KEY (materia_inicial) REFERENCES public.subjects(id)
);