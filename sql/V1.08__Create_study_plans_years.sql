CREATE TABLE public.study_plans_years (
	id serial4 NOT NULL,
	study_plan_id int4 NULL,
	year_id int4 NULL,
	node_id varchar NULL,
	CONSTRAINT study_plans_years_pk PRIMARY KEY (id),
	CONSTRAINT study_plans_years_fk FOREIGN KEY (study_plan_id) REFERENCES public.study_plans(id),
	CONSTRAINT study_plans_years_fk_1 FOREIGN KEY (year_id) REFERENCES public.years(id)
);