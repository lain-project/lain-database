CREATE TABLE public.na_tokens (
    id SERIAL CONSTRAINT na_tokens_pk PRIMARY KEY,
    access_token VARCHAR NOT NULL,
    token_type VARCHAR NOT NULL,
    user_id INTEGER,
    expires_at TIMESTAMP WITHOUT TIME ZONE
);