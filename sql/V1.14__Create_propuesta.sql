CREATE TABLE public.propuestas (
	id uuid NOT NULL DEFAULT uuid_generate_v4(),
	nodo_id varchar NULL,
	contenido_minimo varchar NULL,
	fundamentos varchar NULL,
	objetivos varchar NULL,
	metodologia varchar NULL,
	evaluacion varchar NULL,
	bibliografia_general varchar NULL,
	fecha_creacion timestamp NULL DEFAULT now(),
	fecha_actualizacion timestamp NULL DEFAULT now(),
	usuario_creador int4 NULL,
	usuario_actualizador int4 NULL,
	materia_id uuid NULL,
	CONSTRAINT propuestas_pk PRIMARY KEY (id),
	CONSTRAINT propuestas_fk FOREIGN KEY (materia_id) REFERENCES public.subjects(id)
);