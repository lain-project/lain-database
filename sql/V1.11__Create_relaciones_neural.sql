CREATE TABLE public.relaciones_neural (
	id uuid NOT NULL,
	nodo_inicial varchar NULL,
	nodo_final varchar NULL,
	CONSTRAINT relaciones_neural_pk PRIMARY KEY (id)
);